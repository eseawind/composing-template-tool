var TitleMenuView = ToolbarView.extend({
	titles:['h1','h2','h3'],
	titleNames:{'h1':'标题1','h2':'标题2','h3':'标题3','h4':'标题4','h5':'标题5'},
	max_height:38,
	events:{
		'click .type-menu':'changeType'
	},
	render_:function(){
		var self = this;
		var type = self.opts.type;
		var type_lis = self.makeTypeMenu(type);
		var type_text = self.titleNames[type];
		var menu_tpl = [
			'<ul class="main-toolbar-wrapper">',
				'<li class="main-toolbar-item select off title">'+type_text,
					type_lis,
				'</li>',
				this.commonTpl,
			'</ul>',
		].join('\n');
		this.$el.html(menu_tpl);
		return this.$el;
	},
	doSelectActions:function(e,select){
		if(select.is('.title')){
			this.changeType(e);
		}
	},
	changeType:function(e){
		var target = $(e.currentTarget);
		var cur_title = this.opts.type;
		var title = target.data('type');
		if(cur_title != title){
			this.opts.type = title;
			var view = ctpl.get('view');
			if(!view)
				return;
			var target = ctpl.get('target');
			view.render(this.opts,true);
			ctpl.set('target',view.main);
		}
	},
	types:function(escape){
		var titles = this.titles;
		titles = $.grep(titles,function(t){return t !== escape});
		return titles;
	},
	makeTypeMenu:function(type){
		var types = this.types(type);
		var lis = [
			'<ul>'
		];
		for(var i=0;i<types.length;i++){
			var type = types[i];
			var li = [
				'<li class="'+types[i]+'" data-type="'+types[i]+'">',
					this.titleNames[type],
				'</li>'
			].join('\n');
			lis.push(li);
		}
		lis.push('</ul>');
		return lis.join('\n');
	}
});

var TitleView = TemplateView.extend({
	classPrefix:'ctpl-title',
	newline:false,
	autoclear:true,
	updateMenu:function(){
		this.menu = new TitleMenuView(this.opts);
	}
});