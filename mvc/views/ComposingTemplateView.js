var MainToolbarView = Backbone.View.extend({
	tagName:'div',
	className:'ctpl-main-toolbar',
	events:{
		'tap .main-toolbar-item.remove':'remove',
		'tap .main-toolbar-item.select':'toggleSelectStatus',
		'tap .main-toolbar-item.select li':'doSelectAction',
		'tap .main-toolbar-item.ok':'ok',
		'doubletap':'ok'
	},
	initialize:function(opt){
		this.opts = _.extend(opt);
	},
	render:function(target,opts){
		var cmt = target.find('.ctpl-main-toolbar');
		var toolbar_tpl = [
			'<ul class="main-toolbar-wrapper">',
				'<li class="main-toolbar-item select off template">模板',
					'<ul>',
						'<li data-type="title">新闻</li>',
						'<li data-type="paragraph">杂文/随笔</li>',
						'<li data-type="release">软件发布</li>',
						'<li data-type="diary">日志</li>',
					'</ul>',				
				'</li>',
				'<li class="main-toolbar-item select off add_item">添加',
					'<ul>',
						'<li data-type="title">标题</li>',
						'<li data-type="paragraph">段落</li>',
						'<li data-type="image">图片</li>',
						'<li data-type="list">列表</li>',
						'<li data-type="quote">引用</li>',
						'<li data-type="catalog">目录</li>',
					'</ul>',				
				'</li>',
				'<li class="main-toolbar-item off preview">预览</li>',
			'</ul>'
		].join('\n');

		if(cmt.length==0){
			this.$el.css({'width':opts.width});
			this.$el.html(toolbar_tpl);
			target.prepend(this.$el);
		}else{
			cmt.css({'width':opts.width});
			cmt.html(toolbar_tpl);
		}
		return this;
	},
	ok:function(e){
		var view = ctpl.get('view');
		if(!view)
			return;
		view.stopEdit(ctpl.get('target'));
	},
	remove:function(e){
		var view = ctpl.get('view');
		if(!view)
			return;
		var words = ['确认删除 [',view.opts.text,']?'].join(' ');
		if(confirm(words)){
			view.remove();
			ctpl.set({
				'target':undefined,
				'view':undefined
			})
		}
	},
	toggleSelectStatus:function(e){
		var self = $(e.target);
		self.toggleClass('on off');
	},
	doSelectAction:function(e){
		var select = $(e.target).parents('.select');
		if(select.is('.add_item')){
			this.addNewItem(e);
		}
		select.toggleClass('on off');
	},
	addNewItem:function(e){
		var self = $(e.target);
		var type = self.data('type');
		if(type){
			var main = this.opts.main;
			main.addView(type,main.moptions()[type]);
		}
	}
});

var ComposingTemplateView = Backbone.View.extend({
	tagName:'div',
	className:'ctpl',
	modules:['title','paragraph','image','quote','list','catalog'],
	moptions:function(){
		return _.clone({
			'title':{
				type:'h1',
				text:'标题1：在此输入标题内容'
			},
			'paragraph':{
				type:'p',
				text:'段落：在此输入段落内容'
			},
			'quote':{
				type:'q',
				text:'引用：在此输入引用内容'
			},
			'image':{
				type:'img',
				text:'<img id="img" src="http://sandbox.runjs.cn/uploads/rs/90/wwourf2n/89964_200.jpg"><div>图片：在此输入图片描述或标题</div>'
			},
			'list':{
				type:'ul',
				text:'<ul><li>列表</li></ul>'
			},
			'catalog':{
				type:'normal',
				text:'<div class="catalog-head on">目录 :</div><div class="catalog-content on"></div>'
			}
		});
	},
	initialize:function(opt){
		var self = this;
		this.opts = _.extend(opt);
		this.render();
		this.renderToolbar();
		ctpl.on('change:target',function(model,target){
			if(!target)
				self.renderToolbar();
		});

	},
	render:function(){
		var container = this.opts.target;
		this.$el.css({'width':this.opts.width});
		container.html(this.$el);
	},
	renderToolbar:function(toolbar){
		if(!toolbar){
			this.toolbar = toolbar = new MainToolbarView({main:this});
		}
		toolbar.render(this.$el,this.opts);
	},
	addView:function(view,options){
		var instance = null;
		if(this.modules.indexOf(view)!=-1){
			ctpl.set({
				'view':undefined,
				'target':undefined
			});
			options = _.extend(options,{target:this.$el,parent:this,name:view,container:this.opts.target});
			switch(view){
				case 'title':
					instance = new TitleView(options);
				break;
				case 'paragraph':
					instance = new ParagraphView(options);
				break;
				case 'image':
					instance = new ImageView(options);
				break;
				case 'quote':
					instance = new QuoteView(options);
				break;
				case 'list':
					instance = new ListView(options);
				break;
				case 'catalog':
					instance = new CatalogView(options);
				break;
			}
			ctpl.set({
				'view':instance,
				'target':instance.main
			});
		}
		return instance;
	},
	toggleLoadingLayout:function(auto_close){
		var self = this;
		var loadingLayout = $('.loading-wrapper');
		if(loadingLayout.length==0 && !auto_close){
			loadingLayout = $('<div class="loading-wrapper"><img src="img/icon/loading.gif"/></div>');
			loadingLayout.on('tap',function(){
				self.toggleLoadingLayout(true)
			});
			loadingLayout.css({
				width:document.width,
				height:document.height,
				display:'none',
				'background-color':'rgba(0,0,0,0.5)',
				'text-align':'center',
				'position':'absolute',
				'z-index':'101'
			})
			this.opts.target.prepend(loadingLayout);
			var img = loadingLayout.find('img');
			img[0].onload = function(){
				img.css({
					'margin-top':($(window).height() - img.height())/2
				});
				loadingLayout.fadeIn(function(){
					setTimeout(self.toggleLoadingLayout,5*1000,true);
				});
			};
		}else{
			loadingLayout.fadeOut(function(){
				$(this).remove();
			});
		}
	}
});
