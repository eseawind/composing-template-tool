var Global = Backbone.Model.extend({
	target:$(document),
	view:null,
	width:function(){
		return $('.ctpl').width();
	},
	invoke:function(method,param){
		var view = this.get('view');
		var target = this.get('target');
		if(!view)
			return;
		method = view[method];
		if(_.isFunction(method)){
			method(target,param);
		}
	}
});